package com.rizkysaraan.moviecatalogue.data;

import android.os.Parcel;
import android.os.Parcelable;

public class DataItem implements Parcelable {
    private String ItemTitle,ItemDateRelease,ItemDesc,ItemDirector,ItemImage;
    private int ItemScore;


    public DataItem(String itemTitle, String itemDateRelease, String itemDesc, String itemDirector, String itemImage, int itemScore) {
        ItemTitle = itemTitle;
        ItemDateRelease = itemDateRelease;
        ItemDesc = itemDesc;
        ItemDirector = itemDirector;
        ItemImage = itemImage;
        ItemScore = itemScore;
    }

    public String getItemTitle() {
        return ItemTitle;
    }

    public void setItemTitle(String ItemTitle) {
        this.ItemTitle = ItemTitle;
    }

    public String getItemDateRelease() {
        return ItemDateRelease;
    }

    public void setItemDateRelease(String ItemDateRelease) {
        this.ItemDateRelease = ItemDateRelease;
    }

    public String getItemDesc() {
        return ItemDesc;
    }

    public void setItemDesc(String ItemDesc) {
        this.ItemDesc = ItemDesc;
    }

    public String getItemDirector() {
        return ItemDirector;
    }

    public void setItemDirector(String ItemDirector) {
        this.ItemDirector = ItemDirector;
    }

    public String getItemImage() {
        return ItemImage;
    }

    public void setItemImage(String ItemImage) {
        this.ItemImage = ItemImage;
    }

    public int getItemScore() {
        return ItemScore;
    }

    public void setItemScore(int ItemScore) {
        this.ItemScore = ItemScore;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ItemTitle);
        dest.writeString(this.ItemDateRelease);
        dest.writeString(this.ItemDesc);
        dest.writeString(this.ItemDirector);
        dest.writeString(this.ItemImage);
        dest.writeInt(this.ItemScore);
    }

    public DataItem() {
    }

    private DataItem(Parcel in) {
        this.ItemTitle = in.readString();
        this.ItemDateRelease = in.readString();
        this.ItemDesc = in.readString();
        this.ItemDirector = in.readString();
        this.ItemImage = in.readString();
        this.ItemScore = in.readInt();
    }

    public static final Parcelable.Creator<DataItem> CREATOR = new Parcelable.Creator<DataItem>() {
        @Override
        public DataItem createFromParcel(Parcel source) {
            return new DataItem(source);
        }

        @Override
        public DataItem[] newArray(int size) {
            return new DataItem[size];
        }
    };
}
