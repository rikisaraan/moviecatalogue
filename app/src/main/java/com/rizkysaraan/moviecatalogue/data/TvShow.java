package com.rizkysaraan.moviecatalogue.data;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class TvShow implements Parcelable {

    String title;
    String first_air_date;
    Double popularity;
    Double vote_count;
    Double vote_average;
    String backdrop_path;
    String overview;
    String poster_path;
    String original_language;

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Double getVote_count() {
        return vote_count;
    }

    public void setVote_count(Double vote_count) {
        this.vote_count = vote_count;
    }

    public Double getVote_average() {
        return vote_average;
    }

    public void setVote_average(Double vote_average) {
        this.vote_average = vote_average;
    }

    public TvShow(JSONObject object) {
        try {

            String title = object.getString("title");
            String first_air_date = object.getString("first_air_date");
            Double popularity = object.getDouble("popularity");
            Double vote_count = object.getDouble("vote_count");
            Double vote_average = object.getDouble("vote_average");
            String backdrop_path = object.getString("backdrop_path");
            String overview = object.getString("overview");
            String poster_path = object.getString("poster_path");
            String original_language = object.getString("original_language");

            this.title = title;
            this.title =title;
            this.first_air_date =first_air_date;
            this.popularity =popularity;
            this.vote_count =vote_count;
            this.backdrop_path =backdrop_path;
            this.overview =overview;
            this.poster_path =poster_path;
            this.original_language=original_language;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TvShow() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.first_air_date);
        dest.writeValue(this.popularity);
        dest.writeValue(this.vote_count);
        dest.writeValue(this.vote_average);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
        dest.writeString(this.original_language);
    }

    protected TvShow(Parcel in) {
        this.title = in.readString();
        this.first_air_date = in.readString();
        this.popularity = (Double) in.readValue(Double.class.getClassLoader());
        this.vote_count = (Double) in.readValue(Double.class.getClassLoader());
        this.vote_average = (Double) in.readValue(Double.class.getClassLoader());
        this.backdrop_path = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
        this.original_language = in.readString();
    }

    public static final Creator<TvShow> CREATOR = new Creator<TvShow>() {
        @Override
        public TvShow createFromParcel(Parcel source) {
            return new TvShow(source);
        }

        @Override
        public TvShow[] newArray(int size) {
            return new TvShow[size];
        }
    };
}
