package com.rizkysaraan.moviecatalogue.data;

import java.util.ArrayList;

public class DataExample {
    private static final String[][] dataMovie = new String[][]{
            {"Venom","1 Oktober 2018","Investigative journalist Eddie Brock attempts a comeback following a scandal, but accidentally becomes the host of Venom, a violent, super powerful alien symbiote. Soon, he must rely on his newfound powers to protect the world from a shadowy organization looking for a symbiote of their own.","Ruben Fleischer","poster_venom","66"},
            {"Avengers: Infinity War ","27 April 2018","As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.","Andi","poster_avengerinfinity","83"},
            {"Bohemian Rhapsody ","2 November 2018","Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.","Bryan Singer","poster_bohemian","81"},
            {"Aquaman","21 Desember 2018","Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.","James Wan","poster_aquaman","68"},
            {"Bumblebee","21 Desember 2018","On the run in the year 1987, Bumblebee finds refuge in a junkyard in a small Californian beach town. Charlie, on the cusp of turning 18 and trying to find her place in the world, discovers Bumblebee, battle-scarred and broken. When Charlie revives him, she quickly learns this is no ordinary yellow VW bug.","Travis Knight","poster_bumblebee","65"},
            {"Spiderman","1 Desember 2018","Miles Morales is juggling his life between being a high school student and being a spider-man. When Wilson \"Kingpin\" Fisk uses a super collider, others from across the Spider-Verse are transported to this dimension.","Rodney Rothman","poster_spiderman","84"},
            {"Deadpool","10 Mei 2018","Wisecracking mercenary Deadpool battles the evil and powerful Cable and other bad guys to save a boy's life.","David Leitch","poster_deadpool","75"},
            {"Mortal Engine","14 Desember 2018","Many thousands of years in the future, Earth’s cities roam the globe on huge wheels, devouring each other in a struggle for ever diminishing resources. On one of these massive traction cities, the old London, Tom Natsworthy has an unexpected encounter with a mysterious young woman from the wastelands who will change the course of his life forever.","Christian Rivers","poster_mortalengine","60"},
            {"Hunter Killer","26 Oktober 2018","Captain Glass of the USS Arkansas discovers that a coup d'état is taking place in Russia, so he and his crew join an elite group working on the ground to prevent a war.","Donovan Marsh","poster_hunterkiller","63"},
            {"BirdBox","12 November 2018","Five years after an ominous unseen presence drives most of society to suicide, a survivor and her two children make a desperate bid to reach safety.","Susanne Bier","poster_birdbox","70"},
            {"Creed II","14 November 2018","Between personal obligations and training for his next big fight against an opponent with ties to his family's past, Adonis Creed is up against the challenge of his life.","Steven Caple Jr.","poster_creed","67"},
            {"The Mule","10 Desember 2018","Earl Stone, a man in his 80s, is broke, alone, and facing foreclosure of his business when he is offered a job that simply requires him to drive. Easy enough, but, unbeknownst to Earl, he’s just signed on as a drug courier for a Mexican cartel. He does so well that his cargo increases exponentially, and Earl hit the radar of hard-charging DEA agent Colin Bates.","Clint Eastwood","poster_themule","68"},

    };

    private static final String[][] dataTvShows = new String[][]{
            {"NCIS","23 September 2003","From murder and espionage to terrorism and stolen submarines, a team of special agents investigates any crime that has a shred of evidence connected to Navy and Marine Corps personnel, regardless of rank or position.","Don McGill","poster_ncis","67"},
            {"Supergirl","26 Oktober 2015","Twenty-four-year-old Kara Zor-El, who was taken in by the Danvers family when she was 13 after being sent away from Krypton, must learn to embrace her powers after previously hiding them. The Danvers teach her to be careful with her powers, until she has to reveal them during an unexpected disaster, setting her on her journey of heroism.","Greg Berlanti","poster_supergirl","58"},
            {"Shameless","9 Januari 2011","Chicagoan Frank Gallagher is the proud single dad of six smart, industrious, independent kids, who without him would be... perhaps better off. When Frank's not at the bar spending what little money they have, he's passed out on the floor. But the kids have found ways to grow up in spite of him. They may not be like any family you know, but they make no apologies for being exactly who they are.","Paul Abbott","poster_shameless","78"},
            {"Supernatural","13 September 2005","When they were boys, Sam and Dean Winchester lost their mother to a mysterious and demonic supernatural force. Subsequently, their father raised them to be soldiers. He taught them about the paranormal evil that lives in the dark corners and on the back roads of America ... and he taught them how to kill it. Now, the Winchester brothers crisscross the country in their '67 Chevy Impala, battling every kind of supernatural threat they encounter along the way.","Eric Kripke","poster_supernatural","73"},
            {"Family Guy","31 Januari 1999","Sick, twisted, politically incorrect and Freakin' Sweet animated series featuring the adventures of the dysfunctional Griffin family. Bumbling Peter and long-suffering Lois have three kids. Stewie (a brilliant but sadistic baby bent on killing his mother and taking over the world), Meg (the oldest, and is the most unpopular girl in town) and Chris (the middle kid, he's not very bright but has a passion for movies). The final member of the family is Brian - a talking dog and much more than a pet, he keeps Stewie in check whilst sipping Martinis and sorting through his own life issues.","Seth MacFarlane","poster_family_guy","65"},
            {"Gotham","22 September 2014","Before there was Batman, there was GOTHAM. Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?","Bruno Heller","poster_gotham","68"},
            {"Naruto Shippūden","15 Februari 2007","Naruto Shippuuden is the continuation of the original animated TV series Naruto.The story revolves around an older and slightly more matured Uzumaki Naruto and his quest to save his friend Uchiha Sasuke from the grips of the snake-like Shinobi, Orochimaru. After 2 and a half years Naruto finally returns to his village of Konoha, and sets about putting his ambitions to work, though it will not be easy, as He has amassed a few (more dangerous) enemies, in the likes of the shinobi organization; Akatsuki.","Masashi Kishimoto","poster_naruto_shipudden","75"},
            {"The Flash","7 Oktober 2014","After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.","Geoff Johns","poster_flash","67"},
            {"The Simpsons","17 Desember 1989","Set in Springfield, the average American town, the show focuses on the antics and everyday adventures of the Simpson family; Homer, Marge, Bart, Lisa and Maggie, as well as a virtual cast of thousands. Since the beginning, the series has been a pop culture icon, attracting hundreds of celebrities to guest star. The show has also made name for itself in its fearless satirical take on politics, media and American life in general.","Matt Groening","poster_the_simpson","71"},
            {"Dragon Ball Z","26 April 1989","Dragon Ball Z is a Japanese animated television series produced by Toei Animation. Dragon Ball Z is the sequel to the Dragon Ball anime and adapts the last 26 volumes of the original 42 volume Dragon Ball manga series created by Akira Toriyama The series Debut in 1988-1995 on Weekly Shounen Jump. Dragon Ball Z depicts the continuing adventures of Goku and his companions to defend against an assortment of villains which seek to destroy or rule the Earth.","Akira Toriyama","poster_dragon_ball","80"},
            {"Arrow","10 Oktober 2012","Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.","Greg Berlanti","poster_arrow","58"},
            {"Game of Thrones","17 April 2011","Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.","David Benioff","poster_god","81"},
    };

    public static ArrayList<DataItem> getListDataMovie(){
        ArrayList<DataItem> list = new ArrayList<>();
        for (String[] aData : dataMovie) {
            DataItem movie = new DataItem();
            movie.setItemTitle(aData[0]);
            movie.setItemDateRelease(aData[1]);
            movie.setItemDesc(aData[2]);
            movie.setItemDirector(aData[3]);
            movie.setItemImage(aData[4]);
            movie.setItemScore(Integer.parseInt(aData[5]));

            list.add(movie);
        }
        return list;
    }

    public static ArrayList<DataItem> getListDataTvShows(){
        ArrayList<DataItem> list = new ArrayList<>();
        for (String[] aData : dataTvShows) {
            DataItem movie = new DataItem();
            movie.setItemTitle(aData[0]);
            movie.setItemDateRelease(aData[1]);
            movie.setItemDesc(aData[2]);
            movie.setItemDirector(aData[3]);
            movie.setItemImage(aData[4]);
            movie.setItemScore(Integer.parseInt(aData[5]));

            list.add(movie);
        }
        return list;
    }
}
