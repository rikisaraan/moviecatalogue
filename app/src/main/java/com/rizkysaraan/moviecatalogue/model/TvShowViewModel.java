package com.rizkysaraan.moviecatalogue.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.rizkysaraan.moviecatalogue.data.Movie;
import com.rizkysaraan.moviecatalogue.data.TvShow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class TvShowViewModel extends ViewModel {
    private static final String API_KEY = "3bed2b73a2b5aab3aab3f072ea851995";
    private MutableLiveData<ArrayList<TvShow>> listTvShow= new MutableLiveData<>();

    public void setTvShow() {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = "https://api.themoviedb.org/3/discover/tv?api_key="+API_KEY+"&language=en-US";
        final ArrayList<TvShow> listItems = new ArrayList<>();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    Log.e("TvShow",list.toString());

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject tvshow = list.getJSONObject(i);
                        TvShow tvShowItems = new TvShow();
                        tvShowItems.setTitle(tvshow.getString("name"));
                        tvShowItems.setFirst_air_date(tvshow.getString("first_air_date"));
                        tvShowItems.setPopularity(tvshow.getDouble("popularity"));
                        tvShowItems.setVote_count(tvshow.getDouble("vote_count"));
                        tvShowItems.setVote_average(tvshow.getDouble("vote_average"));
                        tvShowItems.setBackdrop_path(tvshow.getString("backdrop_path"));
                        tvShowItems.setOverview(tvshow.getString("overview"));
                        tvShowItems.setOriginal_language(tvshow.getString("original_language"));
                        tvShowItems.setPoster_path("https://image.tmdb.org/t/p/w185"+tvshow.getString("poster_path"));

                        listItems.add(tvShowItems);
                    }
                    listTvShow.postValue(listItems);
                } catch (Exception e) {
                    Log.d("Exception", e.getMessage());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public LiveData<ArrayList<TvShow>> getTvShow() {
        return listTvShow;
    }
}
