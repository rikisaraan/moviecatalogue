package com.rizkysaraan.moviecatalogue.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.rizkysaraan.moviecatalogue.data.Movie;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MovieViewModel extends ViewModel {
    private static final String API_KEY = "3bed2b73a2b5aab3aab3f072ea851995";
    private MutableLiveData<ArrayList<Movie>> listMovie= new MutableLiveData<>();

    public void setMovie() {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = "https://api.themoviedb.org/3/discover/movie?api_key="+API_KEY+"&language=en-US";
        final ArrayList<Movie> listItems = new ArrayList<>();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    Log.e("Movie",list.toString());

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject movie = list.getJSONObject(i);
                        Movie movieItems = new Movie(movie);
                        movieItems.setTitle(movie.getString("title"));
                        movieItems.setRelease_date(movie.getString("release_date"));
                        movieItems.setOverview(movie.getString("overview"));
                        movieItems.setPoster_path("https://image.tmdb.org/t/p/w185"+movie.getString("poster_path"));
                        movieItems.setVote_average(movie.getDouble("vote_average"));
                        movieItems.setVote_count(movie.getDouble("vote_count"));
                        movieItems.setPopularity(movie.getDouble("popularity"));
                        movieItems.setOriginal_language(movie.getString("original_language"));
                        movieItems.setBackdrop_path(movie.getString("backdrop_path"));

                        listItems.add(movieItems);
                    }
                    listMovie.postValue(listItems);
                } catch (Exception e) {
                    Log.d("Exception", e.getMessage());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public LiveData<ArrayList<Movie>> getMovie() {
        return listMovie;
    }
}

