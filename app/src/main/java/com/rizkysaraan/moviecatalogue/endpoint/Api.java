package com.rizkysaraan.moviecatalogue.endpoint;

import com.rizkysaraan.moviecatalogue.data.DataItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    public final String API_KEY="3bed2b73a2b5aab3aab3f072ea851995";
    String BASE_URL = "https://api.themoviedb.org/3/discover/";

    @GET("movie?api_key="+API_KEY+"&language=en-US")
    Call<List<DataItem>> getMoviesUpcoming();



}
