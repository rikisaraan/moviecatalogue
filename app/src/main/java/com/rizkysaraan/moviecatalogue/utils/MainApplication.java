package com.rizkysaraan.moviecatalogue.utils;

import android.app.Application;
import android.content.Context;

public class MainApplication extends Application {

    // override the base context of application to update default locale for the application
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, LocaleHelper.getLanguage(base)));
    }

}
