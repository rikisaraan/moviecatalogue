package com.rizkysaraan.moviecatalogue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rizkysaraan.moviecatalogue.R;
import com.rizkysaraan.moviecatalogue.data.TvShow;

import java.util.ArrayList;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.TvShowViewHolder> {
    private ArrayList<TvShow> mData = new ArrayList<>();
    private Context mContext;
    private OnItemClickCallback onItemClickCallback;

    public void setData(ArrayList<TvShow> items) {
        mData.clear();
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public TvShowViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new TvShowViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowViewHolder tvShowViewHolder, int position) {
        tvShowViewHolder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class TvShowViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtOverview ;
        ImageView itemImg;

        TvShowViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.tv_title_item);
            txtOverview = itemView.findViewById(R.id.tv_desc_item);
            itemImg= itemView.findViewById(R.id.imageViewPoster);
        }

        void bind(final TvShow tvShow) {
            txtTitle.setText(tvShow.getTitle());
            txtOverview.setText(tvShow.getOverview());
            Glide.with(itemView.getContext())
                    .load(tvShow.getPoster_path())
                    .into(itemImg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickCallback.onItemClicked(tvShow);
                }
            });
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(TvShow data);
    }
}
