package com.rizkysaraan.moviecatalogue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rizkysaraan.moviecatalogue.R;
import com.rizkysaraan.moviecatalogue.data.DataItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterItem extends RecyclerView.Adapter<AdapterItem.ListViewHolder> {

    final private List<DataItem> listItem;
    private OnItemClickCallback onItemClickCallback;
    final private Context mContext;

    public AdapterItem(@NonNull Context context,List<DataItem> list){
        this.listItem=list;
        this.mContext = context;
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        DataItem item = listItem.get(position);

        Glide.with(this.mContext).load(item.getItemImage()).into(holder.itemImg);

        holder.itemTitle.setText(item.getItemTitle());
        holder.itemDesc.setText(item.getItemDesc());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listItem.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
          return listItem.size();
    }


    public class ListViewHolder extends RecyclerView.ViewHolder{
        final private ImageView itemImg;
        final private TextView itemTitle, itemDesc;

        private ListViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImg= itemView.findViewById(R.id.imageViewPoster);
            itemTitle= itemView.findViewById(R.id.tv_title_item);
            itemDesc= itemView.findViewById(R.id.tv_desc_item);
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(DataItem data);
    }
}
