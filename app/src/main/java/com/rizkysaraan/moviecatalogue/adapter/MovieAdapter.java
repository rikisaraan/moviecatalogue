package com.rizkysaraan.moviecatalogue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rizkysaraan.moviecatalogue.R;
import com.rizkysaraan.moviecatalogue.data.DataItem;
import com.rizkysaraan.moviecatalogue.data.Movie;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {
    private ArrayList<Movie> mData = new ArrayList<>();
    private Context mContext;
    private OnItemClickCallback onItemClickCallback;

    public void setData(ArrayList<Movie> items) {
        mData.clear();
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new MovieViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int position) {
        movieViewHolder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtOverview ;
        ImageView itemImg;

        MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.tv_title_item);
            txtOverview = itemView.findViewById(R.id.tv_desc_item);
            itemImg= itemView.findViewById(R.id.imageViewPoster);
        }

        void bind(final Movie movie) {
            txtTitle.setText(movie.getTitle());
            txtOverview.setText(movie.getOverview());
            Glide.with(itemView.getContext())
                    .load(movie.getPoster_path())
                    .into(itemImg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickCallback.onItemClicked(movie);
                }
            });
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(Movie data);
    }
}
