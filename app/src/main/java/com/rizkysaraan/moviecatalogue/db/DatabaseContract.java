package com.rizkysaraan.moviecatalogue.db;

import android.provider.BaseColumns;

public class DatabaseContract {
    static String TABLE_NAME = "tbmovie";

    static final class MovieColumns implements BaseColumns {
        static String TITLE = "title";
        static String RELEASE_DATE = "release_date";
        static String OVERVIEW = "overview";
        static String VOTE_COUNT = "vote_count";
        static String VOTE_AVERAGE = "vote_average";
        static String POPULARITY = "popularity";
        static String ORIGINAL_LANGUAGE = "original_language";
        static String POSTER_PATH = "poster_path";
        static String BACKDROP_PATH = "backdrop_path";
    }
}
