package com.rizkysaraan.moviecatalogue.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.rizkysaraan.moviecatalogue.db.DatabaseContract.TABLE_NAME;

public class MovieHelper {
    private static final String DATABASE_TABLE = TABLE_NAME;
    private static DatabaseHelper dataBaseHelper;
    private static MovieHelper INSTANCE;
    private static SQLiteDatabase database;

    //untuk mendeklarasikan variabel-nya
    private MovieHelper(Context context) {
        dataBaseHelper = new DatabaseHelper(context);
    }

    //untuk menginisiasi database.
    public static MovieHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MovieHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    //untuk membuka koneksi
    public void open() throws SQLException {
        database = dataBaseHelper.getWritableDatabase();
    }

    //untuk menutup koneksi
    public void close() {
        dataBaseHelper.close();
        if (database.isOpen())
            database.close();
    }

    //untuk mengambil data.
    public Cursor queryAll() {
        return database.query(
                DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " ASC");
    }

    //metode untuk mengambal data dengan id tertentu.
    public Cursor queryById(String id) {
        return database.query(
                DATABASE_TABLE,
                null,
                _ID + " = ?",
                new String[]{id},
                null,
                null,
                null,
                null);
    }

    //metode untuk menyimpan data.
    public long insert(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    //metode untuk memperbaharui data.
    public int update(String id, ContentValues values) {
        return database.update(DATABASE_TABLE, values, _ID + " = ?", new String[]{id});
    }

    //metode untuk menghapus data.
    public int deleteById(String id) {
        return database.delete(DATABASE_TABLE, _ID + " = ?", new String[]{id});
    }
}
