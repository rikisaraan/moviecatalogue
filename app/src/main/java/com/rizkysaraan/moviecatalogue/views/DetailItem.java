package com.rizkysaraan.moviecatalogue.views;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.rizkysaraan.moviecatalogue.R;
import com.rizkysaraan.moviecatalogue.data.Movie;
import com.rizkysaraan.moviecatalogue.data.TvShow;
import com.rizkysaraan.moviecatalogue.utils.LocaleHelper;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;

public class DetailItem extends AppCompatActivity {
    public static final String EXTRA_ITEM = "extra_item";
    private ProgressBar progressBar;
    TextView tv_movie_title,tv_movie_date_release,tv_movie_language,tv_movie_desc,tv_lbl_date_release,tv_user_score,tv_lbl_original_language,tv_overview;
    ImageView movie_image;
    CircularProgressIndicator cp_movie_score;
    String movieTitle;
    String movieDateRelease;
    String movieOriginalLanguage;
    String movieDesc;
    String movieImage;
    Double movieScore;
    String type;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        type = getIntent().getExtras().getString("type","movie");

        if (getSupportActionBar() != null) {
            String title = "DETAIL "+type.toUpperCase();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }

        progressBar = findViewById(R.id.progressBar);
        tv_movie_title = findViewById(R.id.tv_movie_title);
        tv_movie_date_release = findViewById(R.id.tv_movie_date_release);
        tv_movie_language = findViewById(R.id.tv_movie_language);
        tv_movie_desc = findViewById(R.id.tv_movie_desc);
        movie_image = findViewById(R.id.movie_image);
        cp_movie_score = findViewById(R.id.cp_movie_score);
        tv_lbl_date_release = findViewById(R.id.tv_lbl_date_release);
        tv_user_score = findViewById(R.id.tv_user_score);
        tv_lbl_original_language = findViewById(R.id.tv_lbl_original_language);
        tv_overview = findViewById(R.id.tv_overview);

        showLoading(true);

        final Handler handler = new Handler();

        new Thread(new Runnable() {
            public void run() {
                try{
                    Thread.sleep(5000);
                }
                catch (Exception ignored) { }

                handler.post(new Runnable() {
                    public void run() {

                        if(type.equals("movie")){
                            Movie movie = getIntent().getParcelableExtra(EXTRA_ITEM);
                            if(movie!=null){
                                movieTitle = movie.getTitle();
                                movieDateRelease = movie.getRelease_date();
                                movieOriginalLanguage= movie.getOriginal_language();
                                movieDesc = movie.getOverview();
                                movieImage = movie.getPoster_path();
                                movieScore = movie.getVote_average();
                            }


                        }else{
                            TvShow tvShow = getIntent().getParcelableExtra(EXTRA_ITEM);
                            if(tvShow!=null){
                                movieTitle = tvShow.getTitle();
                                movieDateRelease = tvShow.getFirst_air_date();
                                movieOriginalLanguage= tvShow.getOriginal_language();
                                movieDesc = tvShow.getOverview();
                                movieImage = tvShow.getPoster_path();
                                movieScore = tvShow.getVote_average();
                            }
                        }

                        Glide.with(DetailItem.this)
                                .load(movieImage)
                                .into(movie_image);
                        tv_movie_title.setText(movieTitle);
                        tv_movie_date_release.setText(movieDateRelease);
                        tv_movie_language.setText(movieOriginalLanguage);
                        tv_movie_desc.setText(movieDesc);
                        cp_movie_score.setProgress(movieScore, 10);

                        showLoading(false);

                    }
                });
            }
        }).start();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            tv_movie_title.setVisibility(View.GONE);
            tv_movie_date_release.setVisibility(View.GONE);
            tv_movie_language.setVisibility(View.GONE);
            tv_movie_desc.setVisibility(View.GONE);
            movie_image.setVisibility(View.GONE);
            cp_movie_score.setVisibility(View.GONE);
            tv_lbl_date_release.setVisibility(View.GONE);
            tv_user_score.setVisibility(View.GONE);
            tv_lbl_original_language.setVisibility(View.GONE);
            tv_overview.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tv_movie_title.setVisibility(View.VISIBLE);
            tv_movie_date_release.setVisibility(View.VISIBLE);
            tv_movie_language.setVisibility(View.VISIBLE);
            tv_movie_desc.setVisibility(View.VISIBLE);
            movie_image.setVisibility(View.VISIBLE);
            cp_movie_score.setVisibility(View.VISIBLE);
            tv_lbl_date_release.setVisibility(View.VISIBLE);
            tv_user_score.setVisibility(View.VISIBLE);
            tv_lbl_original_language.setVisibility(View.VISIBLE);
            tv_overview.setVisibility(View.VISIBLE);

        }
    }
}
