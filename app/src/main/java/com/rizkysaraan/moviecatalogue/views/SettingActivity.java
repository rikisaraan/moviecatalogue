package com.rizkysaraan.moviecatalogue.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import com.rizkysaraan.moviecatalogue.R;
import com.rizkysaraan.moviecatalogue.utils.LocaleHelper;

public class SettingActivity extends AppCompatActivity {

    final private String mLanguageCodeEn = "en";
    final private String mLanguageCodeIn = "in";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            String title = getResources().getString(R.string.setting);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }

        RadioButton rbBahasa = findViewById(R.id.rbBahasa);
        RadioButton rbEnglish = findViewById(R.id.rbEnglish);

        if(LocaleHelper.getLanguage(this).equals("in")){
            rbBahasa.setChecked(true);
        }else{
            rbEnglish.setChecked(true);
        }

        rbBahasa.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Change Application level locale
                LocaleHelper.setLocale(SettingActivity.this, mLanguageCodeIn);
                //It is required to refresh the activity to reflect the change in UI.
                relaunch(SettingActivity.this);
            }
        });

        rbEnglish.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Change Application level locale
                LocaleHelper.setLocale(SettingActivity.this, mLanguageCodeEn);
                //It is required to refresh the activity to reflect the change in UI.
                relaunch(SettingActivity.this);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    private void relaunch(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
}
