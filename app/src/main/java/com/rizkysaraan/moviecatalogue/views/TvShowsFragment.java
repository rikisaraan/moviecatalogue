package com.rizkysaraan.moviecatalogue.views;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.rizkysaraan.moviecatalogue.R;
import com.rizkysaraan.moviecatalogue.adapter.AdapterItem;
import com.rizkysaraan.moviecatalogue.adapter.MovieAdapter;
import com.rizkysaraan.moviecatalogue.adapter.TvShowAdapter;
import com.rizkysaraan.moviecatalogue.data.DataExample;
import com.rizkysaraan.moviecatalogue.data.DataItem;
import com.rizkysaraan.moviecatalogue.data.Movie;
import com.rizkysaraan.moviecatalogue.data.TvShow;
import com.rizkysaraan.moviecatalogue.model.MovieViewModel;
import com.rizkysaraan.moviecatalogue.model.TvShowViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TvShowsFragment extends Fragment {

    RecyclerView rvItemTvShows;
    private TvShowAdapter adapter;
    private ProgressBar progressBar;
    private TvShowViewModel tvShowViewModel;


    public TvShowsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_shows, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvItemTvShows = view.findViewById(R.id.rv_tv_shows);
        progressBar = view.findViewById(R.id.progressBar);
        adapter = new TvShowAdapter();

        rvItemTvShows.setLayoutManager(new LinearLayoutManager(this.getContext()));
        rvItemTvShows.setAdapter(adapter);

        progressBar = view.findViewById(R.id.progressBar);

        tvShowViewModel = ViewModelProviders.of(this).get(TvShowViewModel.class);
        tvShowViewModel.getTvShow().observe(this, getTvShow);
        tvShowViewModel.setTvShow();

        showLoading(true);

        adapter.setOnItemClickCallback(new TvShowAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(TvShow data) {
                Intent moveWithObjectIntent = new Intent(getActivity(), DetailItem.class);
                moveWithObjectIntent.putExtra(DetailItem.EXTRA_ITEM, data);
                moveWithObjectIntent.putExtra("type", "tvshow");
                startActivity(moveWithObjectIntent);
            }
        });
    }

    private Observer<ArrayList<TvShow>> getTvShow = new Observer<ArrayList<TvShow>>() {
        @Override
        public void onChanged(ArrayList<TvShow> tvShows) {
            if (tvShows != null) {
                adapter.setData(tvShows);
            }

            showLoading(false);

        }
    };

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
